from flask import Flask
from api import api
app = Flask(__name__)
# Register the api Blueprint so that the database api can be registered in the application
# This also allow us to use a separate file for the database apis
app.register_blueprint(api)


@app.route('/')
def hello_world():
    return 'Hello World'


if __name__ == '__main__':
    app.run()