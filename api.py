from flask import jsonify
from flask import request
from flask import Blueprint

# Use Blueprint to enable the database api routes to be be contained in a separate file and
# be registered in the application
api = Blueprint('api', __name__)